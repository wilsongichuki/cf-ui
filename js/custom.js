$(document).ready(function(){
    // Positioning and Scrolling. Leave these here.
    // $('.grid-02').css({'min-height': $(window).height() - 51});
    // $('.grid-03').css({'height': $(window).height() - 89});
    // $('.grid-03').jScrollPane();


    // Load Dashboard on page load
    $(window).load(function(){
        $('.grid-02').load('views/main-business-dash.html');
        $('.breadcrumbDesc').text('DASHBOARD');
    });


    // Main Left Navigation: Load files on click.
    $('ul.leftNav a').each(function(){
        $(this).on('click', function(e){
            e.preventDefault();
            var navLink = $(this).attr('href');
            var breadcrumbDesc = $(this).find('li').text();
            var loadLink = navLink + '.html';
            $('ul.leftNav a li').removeClass('active');
            $(this).find('li').addClass('active');
            $('.grid-02').empty().load(loadLink);
            $('.breadcrumbDesc').empty().text(breadcrumbDesc);
            console.log(loadLink + ' Loaded');
        });
    });


    // Anything after this line can be changed
    // Mixpanel test
    /*$('a').each(function(){
        $(this).click(function(){
            clickedVal = $(this).attr('name');
            mixpanel.track("Buttons Clicked", {
                "button": clickedVal
            });
            console.log(clickedVal);
        });
    });
    */





});
